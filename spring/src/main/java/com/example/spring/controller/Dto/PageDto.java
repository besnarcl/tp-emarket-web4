package com.example.spring.controller.Dto;

import lombok.Data;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Data
// Création d'une page avec un type générique T
public class PageDto<T> {

    private List<T> content;
    private int page;
    private int size;
    private int count;
    private long totalCount;
    private int totalPage;

    // j'ai un objet en entrée I et sort un objet en sorti O
    public static <I, O> PageDto<O> fromDomain(Page<I> page, Function<I, O> domainToDtoMapper) {
        return new PageDto<O>()
                .setContent(
                        page.getContent().stream()
                                .map(domainToDtoMapper)
                                .collect(Collectors.toList()))
                .setPage(page.getPageable().getPageNumber() + 1)
                .setSize(page.getPageable().getPageSize())
                .setCount(page.getNumberOfElements())
                .setTotalPage(page.getTotalPages())
                .setTotalCount(page.getTotalElements());
    }


}
