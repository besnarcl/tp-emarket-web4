package com.example.spring.model;

import lombok.extern.slf4j.Slf4j;

import javax.persistence.*;
import java.util.Objects;

@Slf4j
@Embeddable
public class CartLine {

    @ManyToOne
    @JoinColumn(name = "products_id", referencedColumnName = "id")
    private Product product;
    private int qty;

    //For JPA
    public CartLine() {
    }

    //Constructor
    public CartLine(Product product, int qty) {
        this.product = product;
        this.qty = qty;
    }


    // getters and setters
    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    // getter avec le calcul du prix pour chaque ligne
    public int getPrice(){
        return getProduct().getPrice()* getQty();
    }

    // c'est la base
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CartLine that = (CartLine) o;
        return qty == that.qty && Objects.equals(product, that.product);
    }

    @Override
    public int hashCode() {
        return Objects.hash(product, qty);
    }
}
