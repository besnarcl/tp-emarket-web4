package com.example.spring.controller.Dto;

import com.example.spring.model.Category;
import com.example.spring.model.Product;
import lombok.Data;

@Data
public class ProductDto {

    private Integer id;
    private String name;
    private Integer price;
    private Integer categoryId;

    public static ProductDto toDtoFull(Product product){

        ProductDto productDto = new ProductDto();
        productDto.setId(product.getId());
        productDto.setName(product.getName());
        productDto.setPrice(product.getPrice());
        productDto.setCategoryId(product.getCategory().getId());

        return productDto;
    }

    public static ProductDto toDtoLight(Product product) {

        ProductDto productDto = new ProductDto();
        productDto.setId(product.getId());
        productDto.setName(product.getName());
        productDto.setPrice(product.getPrice());

        return productDto;
    }

    public Product toDomain(){
        Product product = new Product();
        product.setId(this.id); // this pour pointer l'objet
        product.setName(this.name);
        product.setPrice(this.price);

        Category category = new Category();
        category.setId(categoryId);
        product.setCategory(category);

        return product;
    }
}
