package com.example.spring.model;

import lombok.extern.slf4j.Slf4j;

import javax.persistence.*;
import java.util.Objects;


@Slf4j
@Entity
@Table(name = "products") // va chercher la table product dan la Database
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private Integer price;

    @ManyToOne
    @JoinColumn(name = "categories_id")
    private Category category;

    //Controller
    public Product(Integer id, String name, Integer price, Category category) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.category = category;
    }

    //Controller vide needed to jackson
    public Product() {
    }

    //getters and setters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    // la base
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Objects.equals(id, product.id) && Float.compare(product.price, price) == 0 && Objects.equals(name, product.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, price);
    }
}
