package com.example.spring.errors;

public class UnauthorizedQuantityException extends RuntimeException {
    public UnauthorizedQuantityException() {
    }

    public UnauthorizedQuantityException(String message) {
        super(message);
    }

    public UnauthorizedQuantityException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnauthorizedQuantityException(Throwable cause) {
        super(cause);
    }

    public UnauthorizedQuantityException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
