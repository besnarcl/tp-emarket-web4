package com.example.spring.controller.Dto;

import com.example.spring.model.CartLine;
import com.example.spring.model.Product;
import lombok.Data;

@Data
public class CartLineDto {

    private ProductDto product;
    private int qty;

    public static CartLineDto fromDomain(CartLine cartLine){

        CartLineDto cartLineDto = new CartLineDto();
        cartLineDto.setProduct(ProductDto.toDtoLight(cartLine.getProduct()));
        cartLineDto.setQty(cartLine.getQty());
        return cartLineDto;
    }

    public CartLine toDomain(){

        CartLine cartLine = new CartLine();
        cartLine.setProduct(this.product.toDomain());
        cartLine.setQty(this.qty);

        return cartLine;
    }
}
