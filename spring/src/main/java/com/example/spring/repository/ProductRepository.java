package com.example.spring.repository;

import com.example.spring.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer > {

    Optional<Product> findProductByName(String name);

    List<Product> findProductByCategoryId(Integer id);


}
