package com.example.spring.errors;

public class HttpError {

    private String message;

    public HttpError(String message) {
        this.message = message;
    }

    public HttpError(){}

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
