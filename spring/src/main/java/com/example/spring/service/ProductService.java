package com.example.spring.service;

import com.example.spring.controller.Dto.ProductDto;
import com.example.spring.errors.NotFoundException;
import com.example.spring.model.Category;
import com.example.spring.model.Product;
import com.example.spring.repository.CategoryRepository;
import com.example.spring.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@RequiredArgsConstructor
public class ProductService {

    private final ProductRepository productRepository;
    private final CategoryRepository categoryRepository;

    @Transactional
    public Product addProduct(Product product) {
        loadReference(product);
        return productRepository.save(product);
    }

    public void loadReference(Product product){
        Category existingCategory = categoryRepository.findById(product.getCategory().getId())
                .orElseThrow(()-> new NotFoundException("Category does not exist"));

        product.setCategory(existingCategory);
    }

    @Transactional
    public Product updateProduct(Integer id, Product newProduct) {
        Product productToUpdate = productRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Product does not exist"));

        loadReference(newProduct);

        productToUpdate.setName(newProduct.getName());
        productToUpdate.setPrice(newProduct.getPrice());
        productToUpdate.setCategory(newProduct.getCategory());

        return productRepository.save(productToUpdate);
    }

    @Transactional
    public void deleteProduct(Integer id) {
        Product productToRemove = productRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Product does not exist"));

        productRepository.delete(productToRemove);
    }

}
