package com.example.spring.controller;

import com.example.spring.controller.Dto.CartDto;
import com.example.spring.controller.Dto.CartLineDto;
import com.example.spring.errors.NotFoundException;
import com.example.spring.model.Cart;
import com.example.spring.model.CartLine;
import com.example.spring.repository.CartRepository;
import com.example.spring.service.CartService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin
@RestController
public class CartController {

    private final CartRepository cartRepository;
    private final CartService cartService;

    public CartController(CartRepository cartRepository, CartService cartService) {
        this.cartRepository = cartRepository;
        this.cartService = cartService;
    }

    @PostMapping("/carts")
    public ResponseEntity<CartDto> createCart() {

        Cart newCart = cartService.createCart();

        return ResponseEntity
                .created(URI.create("/carts" + newCart.getId()))
                .body(CartDto.fromDomain(newCart));
    }

    // Create or update
    @PostMapping("/cart/{id}/products/{productId}")
    public ResponseEntity<CartLineDto> addCartLine(@PathVariable("id") Integer id, @PathVariable ("productId") Integer productId, @RequestBody CartLineDto cartLineDto) {

        CartLine cartLine = cartService.addCartLine(id, productId, cartLineDto);

        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(CartLineDto.fromDomain(cartLine));
    }

    @GetMapping("/carts")
    public List<CartDto> getAllCarts() {
        return cartRepository.findAll()
                .stream()
                .map(CartDto::fromDomain)
                .collect(Collectors.toList());
    }

    @GetMapping("/carts/{id}")
    public CartDto getCartById(@PathVariable("id") Integer id) {
        return cartRepository.findById(id)
                .map(CartDto::fromDomain)
                .orElseThrow(() -> new NotFoundException("Cart does not exist"));
    }


    @DeleteMapping("/cart/{id}")
    public void deleteCartLine(@PathVariable("id") Integer id) {
        cartService.delete(id);
    }
}
