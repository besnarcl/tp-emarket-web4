CREATE TABLE IF NOT EXISTS categories
(
    id INTEGER GENERATED ALWAYS AS identity PRIMARY KEY ,
    name TEXT NOT NULL UNIQUE
);

CREATE TABLE IF NOT EXISTS products
(
    id    INTEGER GENERATED ALWAYS AS identity PRIMARY KEY,
    name  TEXT NOT NULL UNIQUE,
    price INTEGER NOT NULL,
    categories_id INTEGER NOT NULL REFERENCES categories(id)
);

CREATE TABLE IF NOT EXISTS carts
(
    id        integer generated always as identity primary key,
    createdAt timestamp not null default now()

);

CREATE TABLE IF NOT EXISTS cartlines
(
    carts_id    integer not null references carts (id),
    products_id integer not null references products (id),
    qty        integer not null,
    primary key (carts_id, products_id)
);

--DATA TEST CATEGORY
INSERT INTO categories
VALUES (default, 'Laptops');
INSERT INTO categories
VALUES (default, 'Desktops');
INSERT INTO categories
VALUES (default, 'Monitors');
INSERT INTO categories
VALUES (default, 'Servers');
INSERT INTO categories
VALUES (default, 'Storage');
INSERT INTO categories
VALUES (default, 'Networking');
INSERT INTO categories
VALUES (default, 'Accessories');

--DATA TEST PRODUCTS CAT.1 LAPTOPS
INSERT INTO products
VALUES (default, 'Latitude 3250', 939, 1);
INSERT INTO products
VALUES (default, 'XPS 13', 1119, 1);
INSERT INTO products
VALUES (default, 'Vostro 3500', 599, 1);
INSERT INTO products
VALUES (default, 'Inspiron 15', 679, 1);
INSERT INTO products
VALUES (default, 'Precision 5760', 3299, 1);
INSERT INTO products
VALUES (default, 'Chromebook 3100', 219, 1);
INSERT INTO products
VALUES (default, 'Alienware M15 R6', 1429, 1);
--DATA TEST PRODUCTS CAT.2 DESKTOPS
INSERT INTO products
VALUES (default, 'Optiplex 3080', 709, 2);
INSERT INTO products
VALUES (default, 'Vostro 3681', 569, 2);
INSERT INTO products
VALUES (default, 'Inspiron 24', 589, 2);
INSERT INTO products
VALUES (default, 'XPS Tower', 919, 2);
INSERT INTO products
VALUES (default, 'Precision 3450', 1119, 2);
INSERT INTO products
VALUES (default, 'Alienware Aurora R12', 1119, 2);
--DATA TEST PRODUCTS CAT.3 MONITORS
INSERT INTO products
VALUES (DEFAULT, 'UltraSharp 43 4K', 899, 3);
INSERT INTO products
VALUES (DEFAULT, 'Dell P2722H', 349, 3);
INSERT INTO products
VALUES (DEFAULT, 'Dell S2721HSX', 259, 3);
INSERT INTO products
VALUES (DEFAULT, 'Dell E2422H', 279, 3);
INSERT INTO products
VALUES (DEFAULT, 'Dell C6522QT', 3374, 3);
--DATA TEST PRODUCTS CAT.4 SERVERS
INSERT INTO products
VALUES (DEFAULT, 'PowerEdge T140 Power Server', 599, 4);
INSERT INTO products
VALUES (DEFAULT, 'PowerEdge R240 Rack Server', 639, 4);
INSERT INTO products
VALUES (DEFAULT, 'PowerEdge XR11 Rack Server', 3989, 4);
INSERT INTO products
VALUES (DEFAULT, 'PowerEdge M640 Blade Server', 1989, 4);
--DATA TEST PRODUCTS CAT.5 STORAGE
INSERT INTO products
VALUES (DEFAULT, 'PowerVault ME4 Series', 10429, 5);
INSERT INTO products
VALUES (DEFAULT, 'PowerVault NX Series Network-Attached Appliances', 3309, 5);
INSERT INTO products
VALUES (DEFAULT, 'PowerVault ME484 Storage Expansion Enclosure', 49789, 5);
INSERT INTO products
VALUES (DEFAULT, 'Dell PowerVault RD1000 Removable Disk Storage', 989, 5);
INSERT INTO products
VALUES (DEFAULT, 'Dell EMC PowerProtect DP4400 Appliance', 13929, 5);
--DATA TEST PRODUCTS CAT.6 NETWORKING
INSERT INTO products
VALUES (DEFAULT, 'Dell EMC PowerSwitch N3200 Series', 4429, 6);
INSERT INTO products
VALUES (DEFAULT, 'Dell EMC Networking Virtual Edge Platform 1405', 1719, 6);
INSERT INTO products
VALUES (DEFAULT, 'Dell EMC PowerSwitch N3208PX-ON', 4249, 6);
--DATA TEST PRODUCTS CAT.7 ACCESSORIES
INSERT INTO products
VALUES (DEFAULT, 'Dell Docking Station', 155, 7);
INSERT INTO products
VALUES (DEFAULT, 'Dell Pro Wireless Keyboard and Mouse – KM5221W', 44, 7);
INSERT INTO products
VALUES (DEFAULT, 'Xerox Phaser 6510/DNI Color Laser Printer ', 469, 7);
INSERT INTO products
VALUES (DEFAULT, 'Dell UltraSharp Webcam', 199, 7);
INSERT INTO products
VALUES (DEFAULT, 'Dell Pro Stereo Headset - WH3022', 51, 7);
INSERT INTO products
VALUES (DEFAULT, 'Seagate 2TB External Hard Drive STEA2000400 Black', 72, 7);