package com.example.spring.errors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice // exception appliquée sur tous les controllers
public class ErrorControllerAdvice {

    private static final Logger LOGGER = LoggerFactory.getLogger(ErrorControllerAdvice.class);

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<HttpError> handleNotFoundException(NotFoundException exception) {
        HttpError httpError = new HttpError();
        httpError.setMessage(exception.getMessage());
        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(httpError);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<HttpError> handleUnexpectedException(Exception exception){
        LOGGER.error("Unexpected Http error", exception);
        HttpError httpError = new HttpError();
        httpError.setMessage("Internal server error");
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(httpError);
    }

    @ExceptionHandler(UnauthorizedQuantityException.class)
    public ResponseEntity<HttpError> handleQuantityException(UnauthorizedQuantityException exception){
        HttpError httpError = new HttpError();
        httpError.setMessage(exception.getMessage());
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(httpError);
    }
}
