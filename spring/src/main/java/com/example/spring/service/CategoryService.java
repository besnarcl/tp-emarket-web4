package com.example.spring.service;

import com.example.spring.controller.Dto.CategoryDto;
import com.example.spring.errors.NotFoundException;
import com.example.spring.model.Category;
import com.example.spring.repository.CategoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@RequiredArgsConstructor
public class CategoryService {

    private final CategoryRepository categoryRepository;

    @Transactional
    public Category addCategory(Category category) {
        return categoryRepository.save(category);
    }

    @Transactional
    public Category updateCategory(Integer id, CategoryDto categoryDto){
        Category categoryToUpdate = categoryRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Category does not exist"));

        categoryToUpdate.setName(categoryDto.getName());

        return categoryRepository.save(categoryToUpdate);
    }

    @Transactional
    public void deleteCategory(Integer id){
        Category categoryToRemove = categoryRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Category does not exist"));

        categoryRepository.delete(categoryToRemove);
    }


}
