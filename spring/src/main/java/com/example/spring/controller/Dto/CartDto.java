package com.example.spring.controller.Dto;

import com.example.spring.model.Cart;
import com.example.spring.model.CartLine;
import lombok.Data;

import java.time.Instant;
import java.util.Set;

@Data
public class CartDto {

    private Integer id;
    private Instant createdAt;
    private Set<CartLine> cartLines;

    public static CartDto fromDomain(Cart cart){

        CartDto cartDto = new CartDto();
        cartDto.setId(cart.getId());
        cartDto.setCreatedAt(cart.getCreatedAt());
        cartDto.setCartLines(cart.getCartLines());

        return cartDto;
    }
}
