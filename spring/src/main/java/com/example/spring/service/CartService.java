package com.example.spring.service;

import com.example.spring.controller.Dto.CartLineDto;
import com.example.spring.errors.NotFoundException;
import com.example.spring.model.Cart;
import com.example.spring.model.CartLine;
import com.example.spring.model.Product;
import com.example.spring.repository.CartRepository;
import com.example.spring.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.Instant;

@Service
@RequiredArgsConstructor  // créer constructor sans que l'on est à l'écrire
public class CartService {

    private final CartRepository cartRepository;
    private final ProductRepository productRepository;



    @Transactional
    public Cart createCart(){
        return cartRepository.save(new Cart(Instant.now()));
    }

    @Transactional
    public CartLine addCartLine(int id, int productId, CartLineDto cartLineDto) {

        //find cart
        Cart cart = cartRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Cart does not exist"));

        // product
        Product product = productRepository.findById(productId)
                .orElseThrow(() -> new NotFoundException("Product does not exist"));

        return cart.addProduct(product, cartLineDto.getQty());
    }

    @Transactional
    public void delete(int id){
        Cart cart = cartRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Cart does not exist"));

        cartRepository.delete(cart);
    }
}
