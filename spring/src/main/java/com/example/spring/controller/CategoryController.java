package com.example.spring.controller;

import com.example.spring.controller.Dto.CategoryDto;
import com.example.spring.errors.NotFoundException;
import com.example.spring.model.Category;
import com.example.spring.repository.CategoryRepository;
import com.example.spring.repository.ProductRepository;
import com.example.spring.service.CategoryService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

@CrossOrigin
@RestController
public class CategoryController {

    private final CategoryRepository categoryRepository;
    private final CategoryService categoryService;
    private final ProductRepository productRepository;

    public CategoryController(CategoryRepository categoryRepository, CategoryService categoryService, ProductRepository productRepository) {
        this.categoryRepository = categoryRepository;
        this.categoryService = categoryService;
        this.productRepository = productRepository;
    }

    //create
    @PostMapping("/categories")
    public ResponseEntity<Category> addCategory(@RequestBody CategoryDto categoryDto) {
        Category categoryToCreate = categoryDto.toDomain();
        categoryService.addCategory(categoryToCreate);

        return ResponseEntity
                .created(URI.create("/categories/" + categoryToCreate.getId()))
                .body(categoryToCreate);
    }

    //get all categories
    @GetMapping("/categories")
    public List<CategoryDto> findAllCategories() {
        List<Category> categories = categoryRepository.findAll();

        List<CategoryDto> categoriesDto = getCategoryDtos(categories);
        return categoriesDto;
    }

    private List<CategoryDto> getCategoryDtos(List<Category> categories) {
        List<CategoryDto> categoriesDto = new ArrayList<>();
        for (Category c : categories) {
            categoriesDto.add(CategoryDto.fromDomain(c)); // convertir c en categoryDto
        }
        return categoriesDto;
    }

    //get by name
    @GetMapping("/categories/")
    public CategoryDto findCategoryByName(@RequestParam("name") String name) {

        return categoryRepository.findCategoryByName(name)
                .map(CategoryDto::fromDomain)
                .orElseThrow(() -> new NotFoundException("Category does not exist"));
    }

    //get by id
    @GetMapping("/categories/{id}")
    public CategoryDto findById(@PathVariable("id") Integer id) {
        return categoryRepository.findById(id)
                .map(CategoryDto::fromDomain)
                .orElseThrow(() -> new NotFoundException("Category does not exist"));
    }

    //update
    @PutMapping("/categories/{id}")
    public ResponseEntity<CategoryDto> updateCategory(@PathVariable("id") Integer id, @RequestBody CategoryDto categoryDto) {

        Category categoryToUpdate = categoryService.updateCategory(id, categoryDto);

        return ResponseEntity
                .status(HttpStatus.ACCEPTED)
                .body(CategoryDto.fromDomain(categoryToUpdate));
    }

    //delete
    @DeleteMapping("/categories/{id}")
    public void deleteCategory(@PathVariable("id") Integer id) {
        categoryService.deleteCategory(id);
    }
}
