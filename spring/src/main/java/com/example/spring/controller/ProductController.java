package com.example.spring.controller;

import com.example.spring.controller.Dto.PageDto;
import com.example.spring.controller.Dto.ProductDto;
import com.example.spring.errors.NotFoundException;
import com.example.spring.model.Product;
import com.example.spring.repository.ProductRepository;
import com.example.spring.service.ProductService;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.SortDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin
public class ProductController {

    private final ProductRepository productRepository;
    private final ProductService productService;

    //Controller
    public ProductController(ProductRepository productRepository, ProductService productService) {
        this.productRepository = productRepository;
        this.productService = productService;
    }

    //create
    @PostMapping("/products")
    public ProductDto addProduct(@RequestBody ProductDto productDto) {

        return ProductDto.toDtoFull(productService.addProduct(productDto.toDomain()));
    }

    //Get all products
    @GetMapping("/products")
    public PageDto<ProductDto> getAllProducts(@SortDefault(value = {"id"})Pageable pageable){

        return PageDto.fromDomain(productRepository.findAll(pageable),ProductDto::toDtoFull);

    }


    // Get by name
    @GetMapping("/products/")
    public ProductDto findProductByName(@RequestParam("name") String name){

        return productRepository.findProductByName(name)
                .map(ProductDto::toDtoFull)
                .orElseThrow(() -> new NotFoundException("Product does not exist"));

    }

    // Get by id
    @GetMapping("/products/{id}")
    public ProductDto findById(@PathVariable ("id") Integer id){

        return productRepository.findById(id)
                .map(ProductDto::toDtoFull)
                .orElseThrow(() -> new NotFoundException("Product does not exist"));
    }

    // Update
    @PutMapping("/products/{id}")
    public ResponseEntity<ProductDto> updateProduct(@PathVariable Integer id, @RequestBody ProductDto productDto){

        Product productToUpdate = productService.updateProduct(id, productDto.toDomain());

       return ResponseEntity
               .status(HttpStatus.ACCEPTED)
               .body(ProductDto.toDtoFull(productToUpdate));
    }

    //delete
    @DeleteMapping("/products/{id}")
    public void deleteProduct (@PathVariable ("id") Integer id) {
        productService.deleteProduct(id);
    }
}
