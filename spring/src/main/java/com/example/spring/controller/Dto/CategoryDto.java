package com.example.spring.controller.Dto;

import com.example.spring.model.Category;
import lombok.Data;

import java.util.List;
import java.util.stream.Collectors;

@Data
public class CategoryDto {

    private Integer id;
    private String name;


    private List<ProductDto> productList;

    public static CategoryDto fromDomain(Category category){

        CategoryDto categoryDto = new CategoryDto();
        categoryDto.setId(category.getId());
        categoryDto.setName(category.getName());

       categoryDto.setProductList(category.getProductList()
                .stream()
                .map(ProductDto::toDtoLight)
                .collect(Collectors.toList()));

        return categoryDto;
    }

    public Category toDomain(){
        Category category = new Category();
        category.setId(this.id);
        category.setName(this.name);
        category.setProductList(this
                .getProductList()
                .stream()
                .map(ProductDto::toDomain)
                .collect(Collectors.toList()));

        return category;
    }
}
