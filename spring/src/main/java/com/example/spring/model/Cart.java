package com.example.spring.model;


import com.example.spring.errors.UnauthorizedQuantityException;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.*;
import java.time.Instant;
import java.util.Set;

@Slf4j //permet enlever logger
@Entity
@Table(name = "carts")
public class Cart {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "createdat")
    private Instant createdAt;

    @ElementCollection(fetch = FetchType.EAGER)
    //pour que les lignes du paniers soient tout le temps chargées quand panier récupéré
    @CollectionTable(name = "cartlines",
            joinColumns = @JoinColumn(name = "carts_id", referencedColumnName = "id"))
    private Set<CartLine> cartLines;

    // appear message in console when Springboot runs - for JPA
    public Cart() {
    }

    //Controller
    public Cart(Integer id, Instant createdAt) {
        this.id = id;
        this.createdAt = createdAt;
    }

    //Controller with only date
    public Cart(Instant createdAt) {
        this.createdAt = createdAt;
    }

    //getters and setters
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public Set<CartLine> getCartLines() {
        return cartLines;
    }

    public void setCartLines(Set<CartLine> cartLines) {
        this.cartLines = cartLines;
    }

    //get cartlines by product
    public CartLine getCartLinesByProduct(Product product) {
        for (CartLine cl : cartLines) {
            if (cl.getProduct().equals(product)) {
                return cl;
            }
        }
        return null;
    }

    // get cartline by Id
    public CartLine getCartLineById(Integer id) {
        for (CartLine pl : cartLines) {
            if (pl.getProduct().getId() == id) {
                return pl;
            }
        }
        return null;
    }

    // get cartline by product name
    public CartLine getCartLineByProductName(String name) {
        for (CartLine pl : cartLines) {
            if (pl.getProduct().getName().equals(name)) {
                return pl;
            }
        }
        return null;
    }

    public CartLine addProduct(Product product, Integer quantity) {
        CartLine cartLine = getCartLinesByProduct(product);

        if (cartLine != null) {
            if (quantity <= 0) {
                throw new UnauthorizedQuantityException("Quantity must be positive");
            } else {
                cartLine.setQty(quantity);
                return cartLine;
            }
        } else {
            CartLine newCartLine = new CartLine(product, quantity);
            getCartLines().add(newCartLine);
            return newCartLine;
        }
    }
}

